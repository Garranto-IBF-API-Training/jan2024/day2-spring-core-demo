package com.garranto.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.garranto")
public class BeanConfigurations {


}

//scan for the classes with @Component annoatations

//    @Bean(value = "accOne")
//    public Account accountOne(){
//        return new Account();
//    }
//
//    @Bean(value = "accTwo")
//    public Account accountTwo(){
//        return  new Account("AC10013",4000,"CREDIT");
//    }

