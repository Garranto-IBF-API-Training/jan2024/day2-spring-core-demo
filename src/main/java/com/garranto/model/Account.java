package com.garranto.model;

import org.springframework.stereotype.Component;

@Component(value = "accOne")
//account
public class Account {

    private String accountNumber;
    private double balance;

    private String type;


    public Account(){
        System.out.println("Account is created using no-arg constructor");
    }

    public Account(String accountNumber, double balance, String type) {
        System.out.println("Account is created using constructor with 3-arguments");
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.type = type;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                ", type='" + type + '\'' +
                '}';
    }
}

//Bean Preprocessors

//primary phylosophy ---  di and loose-coupling
//annotations
//spring mvc
