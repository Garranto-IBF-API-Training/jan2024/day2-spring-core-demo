package com.garranto;

import com.garranto.config.BeanConfigurations;
import com.garranto.model.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

//        creating a context / instatiating the IoC container using xml
//        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

//        instantiating the context using java config
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigurations.class);
        Account accountOne =  context.getBean("accOne", Account.class);
        System.out.println(accountOne);

    }
}


//1. create a application context (Ioc container) using the xml metadata
//2. get the required beans from the context


//    Account accountOne =  context.getBean("accOne", Account.class);
//    Account accountTwo = context.getBean("accTwo", Account.class);
////
//        System.out.println(accountOne);
//                System.out.println(accountTwo);